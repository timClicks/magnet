setup:
	cargo install just
	rustup target add x86_64-unknown-linux-musl
	rustup toolchain install nightly
	rustup component add clippy
	rustup component add rustfmt
	rustup update

build:
	cargo +nightly build --release

clean:
	cargo clean

lint:
	cargo fmt
	cargo +nightly check --workspace --all-targets
	cargo +nightly clippy --workspace --all-targets
