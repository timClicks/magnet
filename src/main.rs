#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;

use std::io::Read;
use std::{collections::HashMap, net::SocketAddr};

use rocket::http::RawStr;
use rocket::http::Status;
use rocket::request::{self, FromRequest, Request};
use rocket::response::{content, status};
use rocket::Outcome;
// use serde_urlencoded;
use regex::{self};

use rusqlite::{Connection, Result};

struct SafeIdentifiers;

#[derive(Debug)]
enum IdentifierError {
    Unsafe,
    TooShort,
    TooLong,
}

fn is_safe_identifier(text: &RawStr) -> bool {
    let ascii_letters = regex::Regex::new(r"^[a-z][\w_-]+$").unwrap();

    ascii_letters.is_match(text)
}

impl<'a, 'r> FromRequest<'a, 'r> for SafeIdentifiers {
    type Error = IdentifierError;

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        let mut p = 1;
        while let Some(param) = request.get_param(p) {
            let text: &RawStr = param.unwrap_or_else(|_| "".into());
            eprint!("{} ", text);
            if text.is_empty() {
                eprintln!("empty!");
                return Outcome::Failure((Status::BadRequest, IdentifierError::TooShort));
            }
            if text.len() > 120 {
                eprintln!("too many!");
                return Outcome::Failure((Status::BadRequest, IdentifierError::TooLong));
            }
            if !is_safe_identifier(text) {
                eprintln!("failed regex!");
                return Outcome::Failure((Status::BadRequest, IdentifierError::Unsafe));
            }
            p += 1;
        }

        Outcome::Success(SafeIdentifiers {})
    }
}

#[put("/db/<name>")]
fn create_database(name: &RawStr, _safe_ids: SafeIdentifiers) -> Result<status::Created<String>> {
    let filename = format!("{}.db", name);
    Connection::open(filename)?;

    Ok(status::Created(format!("/db/{}", name), None))
}

#[get("/db/<name>")]
fn get_database(name: &RawStr) -> Result<String, rusqlite::Error> {
    let filename = format!("{}.db", name);
    let conn = Connection::open(filename)?;

    let mut statement = conn.prepare(
        "
    SELECT sql 
    FROM sqlite_master 
    WHERE name = ?;
    ",
    )?;
    let mut rows = statement.query(&[name.as_bytes()])?;

    #[allow(clippy::never_loop)]
    while let Some(row) = rows.next()? {
        let data = row.get(0)?;
        return Ok(data);
    }
    Ok("".to_string())
}

#[post("/db/<name>/<table>", data = "<row>")]
fn post_to_database(
    name: String,
    table: String,
    row: rocket::Data,
    remote: SocketAddr,
    _safe_ids: SafeIdentifiers,
) -> Result<status::Created<String>, Box<dyn std::error::Error>> {
    let mut fingers_crossed = String::new();
    let mut stream = row.open().take(0x1000);
    stream.read_to_string(&mut fingers_crossed)?;

    let filename = format!("{}.db", name);
    let connection = Connection::open(filename)?;

    let mut create_table = connection.prepare(&format!(
        "
        CREATE TABLE IF NOT EXISTS '{table}' (
            'uuid' TEXT UNIQUE NOT NULL PRIMARY KEY,
            'created_at' DEFAULT CURRENT_TIMESTAMP,
            'remote' TEXT,
            'data' TEXT
        );

        CREATE INDEX IF NOT EXISTS '{table}_uuid_idx' ON {table} (
            'uuid'
        );

        CREATE INDEX IF NOT EXISTS '{table}_created-at_idx' ON {table} (
            'created-at'
        );
    ",
        table = table
    ))?; // TODO: SQL escape table
    create_table.execute([])?;
    // create_table.next()?;

    let uuid_buffer = &mut uuid::Uuid::encode_buffer();
    let rand_uuid = uuid::Uuid::new_v4();
    rand_uuid.to_simple().encode_lower(uuid_buffer);

    let mut insert = connection.prepare(&format!(
        "
    INSERT INTO '{table}' (uuid, remote, data)
    VALUES (?, ?, ?)
    ",
        table = table
    ))?;
    let _row_id = insert.insert(&[
        uuid_buffer.as_ref(),
        remote.to_string().as_bytes(),
        fingers_crossed.as_bytes(),
    ])?;

    Ok(status::Created("".to_string(), None))
}

#[get("/db/<name>/<table>")]
fn export_data(
    name: String,
    table: String,
    _safe_ids: SafeIdentifiers,
) -> Result<content::Json<String>, Box<dyn std::error::Error>> {
    let filename = format!("{}.db", name);
    let connection = Connection::open(filename)?;

    let raw_stmt = format!(
        "
    SELECT created_at, data
    FROM '{table}'
    ",
        table = table
    );

    let mut select = connection.prepare(&raw_stmt)?;
    let mut rows = select.query([])?;

    let mut all_rows = vec![];
    while let Some(row) = rows.next()? {
        let created_at: Box<str> = row.get_unwrap(0);
        let raw_data: Vec<u8> = row.get_unwrap(1);
        let row_decoded: HashMap<String, String> = serde_urlencoded::from_bytes(&raw_data)?;

        let row_ = serde_json::json!({
            "_": serde_json::json!({
                "created_at" : created_at,
            }),
            "d" : row_decoded,
        });

        all_rows.push(row_);
    }

    Ok(content::Json(serde_json::to_string(&all_rows)?))
}

#[get("/")]
fn index() -> &'static str {
    "Hello"
}

fn main() {
    rocket::ignite()
        .mount(
            "/",
            routes![
                index,
                create_database,
                get_database,
                post_to_database,
                export_data
            ],
        )
        .launch();
}
