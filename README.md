                          .
    ,-,-. ,-. ,-. ,-. ,-. |- 
    | | | ,-| | | | | |-' |  
    ' ' ' `-^ `-| ' ' `-' `' 
               ,|            
               `'            

`magnet`: a tiny server for collecting data.
Small enough to work with any shell script, reliable enough to be expected in every shell script.

Currently a small prototype with big dreams.
At this stage, this project is alpha quality software with sharp edges.

# Installation

You will require an installation of Rust.

```console
$ git clone https://gitlab.com/timClicks/magnet
$ cd magnet
$ cargo +nightly run --release
```

# Usage

## Loading data

Once a server is running, it will receive arbitrary data via HTTP.
SQLite databases and tables are created on-demand as data is PUSHed to `/db/<database>/<table>`.

Example:

```console
$ curl localhost:8000/db/air-quality/sensor-abc -d 'pm10=10.21' -d 'pm2.5=12.31'
$ curl localhost:8000/db/air-quality/sensor-abc -d 'pm10=10.22' -d 'pm2.5=12.32'
```

magnet is schemaless.
Feel free to mix and match data types as your use case requires.

## Retrieving data

To retrieve stored data, submit a GET request to the table that you've been sending data to. To improve formatting, the output is piped to a JSON formatter.

```console
$ curl localhost:8000/db/air-quality/sensor-abc | python -m json.tool
[
    {
        "_": {
            "created_at": "2021-04-27 08:58:02"
        },
        "d": {
            "pm10": "10.21",
            "pm2.5": "12.31"
        }
    },
    {
        "_": {
            "created_at": "2021-04-27 08:58:08"
        },
        "d": {
            "pm10": "10.22",
            "pm2.5": "12.32"
        }
    }
]
```

## Analytics/Queries

TODO

## Data Export

TODO

# Contributing

`magnet` is a *very* young project. There are lots of opportunities to help.

## Documentation

It would be helpful to provide add more thorough docs over time.
In particular, describing why someone would pick `magnet` over one of many other tools that perform similar functionality. 

## Testing and feedback

Some questions that need answering:

- what should the API look like?
- how important are the following features:
  - storage size: e.g. should data be compressed at rest?
  - analytics: how to enable analytical queries?
  - progressive schema: magnet should be able to be more sophisticated when the data types are pre-defined
  - auth: both at data ingress and data egress
  - simple deployment: single binary install seems like a win?

## Development

To start adding features, you should be able to run `just setup` from the root of the project. That will install any development dependencies that you'll need for your local dev environment.

- `cargo install just`
- `just setup`
